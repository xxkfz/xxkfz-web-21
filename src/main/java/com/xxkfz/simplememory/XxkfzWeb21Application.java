package com.xxkfz.simplememory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XxkfzWeb21Application {

    public static void main(String[] args) {
        SpringApplication.run(XxkfzWeb21Application.class, args);
    }

}
