package com.xxkfz.simplememory.test;

import lombok.SneakyThrows;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @program: xxkfz-web-21
 * @ClassName MyTest.java
 * @author: wusongtao
 * @create: 2023-11-07 15:45
 * @description:
 * @Version 1.0
 **/
public class MyTest {

    @SneakyThrows
    public static void main(String[] args) {
        long l = System.currentTimeMillis();
//        ExecutorService executor = Executors.newFixedThreadPool(1000); //10145
        ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();//1486
        for (int i = 0; i < 10000; i++) {
            executor.execute(() -> {
                System.out.println(Thread.currentThread());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        }
        executor.shutdown();
        executor.awaitTermination(100, TimeUnit.SECONDS);
        System.out.println(System.currentTimeMillis() - l);
    }

}
